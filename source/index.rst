Les recettes de Kpym
====================

.. toctree::
   :maxdepth: 2

   entrees
   plats
   desserts
   autres

-------

La version `PDF` est disponible à l'adresse http://readthedocs.org/projects/recettes/downloads/pdf/latest/.
