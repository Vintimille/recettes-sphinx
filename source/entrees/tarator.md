# Tarator (soupe de yaourt bulgare)

## Ingrédients
- yaourts : 4
- concombre : 1
- gousse d'ail : 1
- noix : 30 g
- lait : 100 ml
- sel : 1 mg
- aneth : 1/2 botte
- huile d'olives : 3 cuillères

## Temps
- préparation : 25 min
- cuisson : -

## Préparation

On écrase l'ail avec le sel dans le mortier. On rajoute le concombre coupé en petits cubes et on le bat avec l'ail écrasé. On le mélange avec le reste (les noix écrasées, le lait selon les goûts, pas trop de l'huile d'olives).
