# Éclairs au chocolat

## Ingrédients

### Pour la crème pâtissière

- lait : 25 cl
- chocolat noir : 40 g
- jaunes d'œufs : 2
- sucre en poudre : 25 g
- farine : 20 g
- maïzena : 15 g
- beurre : 10 g

### Pour la pâte à choux (450g + 250ml = 700g) :

- eau : 25 cl _(on peut remplacer 5cl par du lait)_
- sel : 3 pincées
- sucre : 5 g
- beurre : 100 g
- farine : 140 g
- œufs : 4
- jaune d'œuf : 1 _(pour badigeonner)_

### Nappage

100 g de chocolat noir

## Temps
- préparation : 1h
- cuisson : 25-30 min

## Préparation

### La crème

Faire fondre le chocolat cassé en morceaux dans le lait, à feu doux.
Dans un bol, fouetter les jaunes d'œuf et le sucre jusqu'à ce que le mélange soit mousseux.
Ajouter la farine et la maïzena et mélanger. Verser le lait chocolaté. Mélanger. Reversez dans la casserole.
Placez la casserole sur le feu doux en remuant toujours au fouet. Porter à ébullition en fouettant sans cesse et cuire ~3 minutes.
Hors du feu, intégrer 20 g de beurre.
Refroidir la crème au réfrigérateur.

### La pâte à choux

Préchauffer le four à 210°C (th. 7)
Couper le beurre en petits morceaux puis le mettre à bouillir avec les liquides, le sel et le sucre.
Tamiser la farine, quand le beurre soit fondu l'ajouter en une fois, hors du feu, et remuer vivement. Cuire la panade obtenue à feu moyen encore quelques minutes pour bien la dessécher.
Refroidir (légèrement) ensuite la préparation, puis ajouter les œufs 1 par 1 (jusqu'à obtenir un ruban un peu cassant lorsqu'on lève la cuillère). La pâte est prête lorsqu'elle tombe de la cuillère en une pâte ferme mais souple.
A l'aide d'une poche à douille, coucher le mélange en boudins de 5 cm de long, les dorer avec un jaune d'œufs passé au pinceau, puis les cuire 21 min dans un four à 210°C. À mi-cuisson, ouvrir la porte très rapidement pour laisser échapper l'humidité (mais sans refroidir le four). Laisser au four 10 min à 150°C, pour continuer à sécher la pâte et éviter que les choux ou les éclairs ne dégonflent.

### Garnir et décorer

Faire fondre le chocolat, puis tremper les éclairs délicatement et les laisser prendre au réfrigérateur 10 min.

## Notes
- Pour que la pâte à choux ne retombe pas (dû à l'humidité enfermée dedans) il y a différentes solutions :
    - bien cuire (10 min de plus) la pâte avant d'incorporer les œufs pour qu'elle se dessèche;
    - cuire à four tout légèrement ouvert (coincé une cuillère);
    - cuire à four fort en deuxième (première ?) moitié de cuisson pour «durcir» la pâte (il faut faire une pâte foncé);
    - à mi-cuisson, ouvrir la porte très rapidement pour laisser échapper l'humidité (mais sans refroidir le four).
- Sans le sel la pâte est fade.
- Il reste plusieurs blancs d'œuf qui peuvent être utilisés pour un financier.
