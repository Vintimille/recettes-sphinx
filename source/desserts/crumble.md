# Crumble

## Ingrédients
- des fruits (pommes, abricots, ...) : 700 g
- farine : 250 g
- beurre : 125 g
- sucre : 125 g

## Temps
- préparation : 15 min
- cuisson : 25-30 min

## Préparation

On coupe les fruits et on les dispose dans un plat beurré.
On mélange la farine, le beurre et le sucre pour obtenir une pâte en miettes. Puis on couvre les fruits avec ces miettes.

## Cuisson

À 180°C : 30 min

## Note
On peut remplacer une partie (50g - 100g) avec poudre d'amandes et/ou de noix de coco _(en fonction des fruits)_.

On peut aussi rajouter :
- des noix
- du cognac
- de la cannelle (si c'est des pommes)
- du citron
