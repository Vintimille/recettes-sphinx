# Banoffee

## Ingrédients

- biscuits (Sables de Flandres) : 200 g
- sucre roux : 30 g
- beurre (½ sel) : 50 g
- lait concentré sucré : 1 boite
- bananes : 3
- crème 30% pour chantilly : 30 cl

## Temps
- préparation : 5 min + 20 min
- cuisson : 40 min

## Préparation

Caramel [en avance] : Faire cuire la boite de lait concentré, sans l'ouvrir, dans une cocote-minute pendant 40 minutes.

Fondre le beurre au micro-onde (30 sec). Écraser les biscuits, les mélanger avec le beurre fondu et le sucre. Mettre dans un cercle, couper les bananes par-dessus, couvrir du caramel. Mettre au frigo. Servir couvert de chantilly.

## Cuisson

Cocote-minute : 40 min (le caramel)

