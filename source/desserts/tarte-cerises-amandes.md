# Tarte cerises et amandes

## Ingrédients

### pour la pâte sablé
- beurre : 100 g
- sucre : 100 g
- farine : 200 g
- jaune d'œuf : 1

### pour la ganache
- beurre : 75 g
- sucre : 100 g
- œufs : 3 (ou 2 + 1 blanc)
- amandes : 100 g
- amandes effilées : 35 g
- sucre glace

### fruits
- cerises (griottes) : 300 g (congelées c'est possible)

## Temps
- préparation : 30 min
- cuisson : 35 min

## Préparation

Faire une pâte sablée (voir la recette). Pendant qu'elle cuit, on fait la ganache :
- 1 min le beurre dans le micro-onde (pour devenir liquide)
- on rajoute le sucre, on mélange
- on rajoute les œufs, on mélange
- on rajoute les amandes, on mélange
- on dispose les cerises griottes (sans les décongeler) sur la la pâte sablé chaude
- on verse la ganache dessus
- on saupoudre des amandes effilées

## Cuisson

À 190°C : 11 min la pâte + 23 min avec le reste.

## Finition

Parsemer de sucre glace.
