# Tarte pistaches-framboises

## Ingrédients

### pour la pâte sablé
- beurre : 100g
- sucre : 100g
- farine : 200g
- jaune d'œuf : 1

### pour la ganache
- beurre : 80g
- sucre : 100g
- œufs : 3 (ou 2 + 1 blanc)
- pistaches (ou 50/50 avec amandes) en poudre : 100g
- farine : 20-30g

### fruits
- framboises : 300g

## Temps
- préparation : 30 min
- cuisson : 35 min

## Préparation

### la pâte sablé
  - 30 sec le beurre dans le micro-onde (pour devenir souple)
  - on rajoute le sucre, on mélange
  - on rajoute le jaune d'œuf, on mélange
  - on rajoute la farine, on mélange avec la cuillère : ça fait une pâte semblable à du «crumble»
  - on dispose dans le plat en distribuant avec la cuillère
  - on cuit 11 min à 190°C

### la ganache
  - 1 min le beurre dans le micro-onde (pour devenir liquide)
  - on rajoute le sucre, on mélange
  - on rajoute les œufs, on mélange
  - on rajoute la pistache, on mélange
  - on verse sur la pâte sablé chaude
  - on cuit 14 min à 190°C

### les fruit
  - on dispose les framboises (surgelés) sur la ganache chaude
  - on cuit encode 10 min (5 min si les framboises sont fraîches)

## Cuisson

À 190°C : 11 min la pâte + 14 min avec la ganache + 10 min avec les fruits

## Remarque
Il semble qu'il existe une recette dans le livre «Les tartes d'Eric Kayser».

