# Tiramisu

## Ingrédients
- mascarpone : 500 g
- œufs : 4
- cuillères a soupe de sucre : 4
- paquet de boudoirs : 1
- café
- lait
- cognac
- cacao (Van Houten)

## Temps
- préparation : 30 min
- cuisson : -

## Préparation

- Le mascarpone + 4 jaunes d'œuf + le sucre + les blancs battus en neige = la crème.
- café au lait + cognac et on trempe les boudoirs dedans. On pose : 1/2 des boudoirs + 1/3 de la crème +  le reste des boudoirs + le reste de la crème + le cacao en poudre. Dans le frigo pendant quelques heurs. Il faut le sortir 1/2 heur avant la consommation.

## Note
- Ne pas trop tremper les boudoirs sinon il reste du liquide au fond.
- Pour l'été on peut remplacer le café au lait par un jus de framboise fraîchement pressé (éventuellement au lait).

