# Financier

## Ingrédients
- poudre d'amande : 50 g
- farine : 50 g
- sucre : 120 g
- beurre : 75 g
- blancs d'œuf : 4
- d'extrait de vanille : quelques goûtes
- sel : 2 pincée

## Temps
- préparation : 15 min
- cuisson : 15-20 min

## Préparation
- On mélange les poudres (amande, farine, sucre, sel).
- On monte les blancs en neige. Puis on les mélange avec les poudres.
- On fait fondre le beurre (45 sec au micro-onde). Puis on l'incorpore au reste avec la vanille.

## Cuisson

À 180°C : 15-20 min

## Note
- Ne pas mélanger le beurre avant d'incorporer les blancs.
- Souvent on fait ces proportions x 3/2.
