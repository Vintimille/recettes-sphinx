# Strudel

## Ingrédients

- pommes : 2-3
- sucre : 75 g
- raisins secs : 75 g
- noix : 75 g
- jus d'un demi-citron
- noix de muscade
- cannelle
- 5 feuilles de pâte filo (ou éventuellement pâte feuilletée pur beur)
- chapelure
- un peu de beurre
- sucre glace


## Finition

Une fois sorti du four, beurrer le dessus puis parsemer de sucre glace.

## Temps
- préparation : 30 min
- cuisson : 35 min

## Préparation

On mélange les pommes coupées en petit dés, le sucre, les raisins secs, les noix, le jus de citron, le noix de muscade et la cannelle. On enroule dans 5 pâtes fillos beurrées et séparé par de la chaplure. On fait cuire.

## Cuisson

À 190°C : 35 min

## Finition

Parsemer de sucre glace.
