# Tarte Tatin

## Ingrédients

### pour la pâte sablé
- beurre : 100 g
- sucre : 100 g
- farine : 200 g
- jaune d'œuf : 1

### pour le caramel
- beurre : 100 g
- sucre : 100 g

### fruits
- pomme golden : 10

## Temps
- préparation : 30 min
- cuisson : 35 min

## Préparation

On épluche et on coupe les pommes (en moitiés ou en cubes). On prépare la pâte sablé. On fait du caramel dans le plat à tarte sur une plaque (100 g de sucre + 100 g de beurre, environs 5 min). On rajoute les pommes (si des moitiés on les met en verticale. On continue à cuire encore 5 min en étouffé. On pose la pâte sable en faisant entrer les bords et en perçant un cheminée au milieu.

## Cuisson

À 180°C : 35 min

## Présentation

À servir tiède avec glace à la vanille, crème fraîche ou chantilly.
