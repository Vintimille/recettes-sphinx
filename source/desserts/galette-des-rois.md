# Galette des rois

## Ingrédients

- pâtes feuilletés _pur beurre_ : 2
- œufs : 2 (+1 jaune pour la finition)
- beurre : 100 g
- sucre : 125 g
- poudre d'amandes (éventuellement des framboises : 125 g
- fruits (des cerises, des pommes, ...) : optionnel
- fève : 1

## Temps

- préparation : 10 min
- cuisson : 30 min

## Préparation

Pour la frangipane : on fait fondre le beurre, on rajoute le sucre, les 2 œufs et la poudre d'amande (éventuellement les fruits). On enferme la frangipane et la fève entre les deux pâtes feuilletées (on fait des rayures au couteau sur le dessus).

## Cuisson

30 min à 190°C (après 25 minutes on badigeonne avec un jaune d'œufs et du sucre mélangés).
