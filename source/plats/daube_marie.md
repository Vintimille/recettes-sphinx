# Daube de bœuf aux raisins secs «Marie»

## Ingrédients

- boeuf : 700 g
- lard fumé : 100 g
- huile : 2 cuillères à soupe
- bon vin rouge : 1/4 litre
- zeste d'orange : 1
- ail : 1 gousse
- oignons : 2 gros
- clous de girofle : 3
- bouquet garni : 1
- sel
- poivre
- thé bouilant : 1 tasse
- grains de coriandre : 7
- raisins secs : 100 g

## Temps
- préparation : 30 min
- cuisson : 2 h

## Préparation
Découpez la viande en gros cubes. Faire revenir dans l'huile bien chaude avec le lards coupé en petits morceaux.
Lorsque la viande est bien dorée, ajoutez le vin + un grand verre d'eau chaude.
Ajouter le zeste d'orange, la gousse d'ail épluchée, les oignons émincés, les clous de girofle, le bouquet garni, sel, poivre. Laisser mijoter à feu doux au moins 2 heures.
Pendant ce temps, faites gonfler les raisins dans un thé noir chaud additionné de poivre et des grains de coriandre: puis laissez macérer.
Egouttez les raisins, ajoutez à la daube, servez bien chaud.
