# Kyouftéta (boulettes de viande bulgares)

## Ingrédients
- blanquette veau haché : 500 g
- oignon : 1
- poivron : 1
- œufs : 2
- tranche de pain : 1
- lait : éventuellement un peu
- sel
- poivre
- persil
- djodjan (menthe verte)
- farine
- huile

## Temps
- préparation : 20 min
- cuisson : 35 min

## Préparation

On trempe le pain dans du lait ou de l'eau. On coupe finement le poivron. Idem pour le persil. On mélange tout (excepté la farine et l'huile). Si le mélange est trop sec on rajoute un peu d'eau. On fait des grosses boulettes qu'on roule dans la farine et on les fait cuire.
