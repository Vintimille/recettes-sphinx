# Pizza «Florent»

## Ingrédients (1 pizza)

### pour la pâte
- farine (type 00) : 200 g
- sel : 2 pincées
- levure sèche boulangère : 1/2 sachet
- eau tiède : 125 g

### pour la garniture
- colis de tomates
- jambon
- fromage râpé ou mozza
...

## Temps
- préparation : 20 min
- cuisson : 10 min

## Préparation
Quelques heures en avance. On fait un puis dans la farine, on met le sel au bord de la jatte, la levure dans le puis, avec un peu d'eau. On commence à malaxer le milieu et plus la boule se forme, plus on rajoute (un peu) d'eau. Une fois la pâte prête on la laisse reposer quelques heures (pas dans le frigo). Cuisson ~10min à 210 C°.
