# Pâte sablée

## Ingrédients

- beur : 100g
- sucre : 100g
- farine : 200g
- jaune d'œuf : 1

## Temps
- préparation : 9 min
- cuisson : 11 min

## Préparation
- 30 sec le beurre dans le micro-onde (pour devenir souple)
- on rajoute le sucre, on mélange
- on rajoute le jaune d'œuf, on mélange
- on rajoute la farine, on mélange avec la cuillère : ça fait une pâte semblable à du «crumble»
- on dispose dans le plat en distribuant avec la cuillère
- on cuit 11 min à 190°C
