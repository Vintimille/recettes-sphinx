# Cumin vs Carvi

## Carvi
(bg) Ким
(de) Kümmel
(en) Caraway

- plus foncé
- plus étoilé
- sans tiges au bout des graines
- utilisation dans l'Europe du Nord
- anisé
- le Guda au cumin est au carvi
- ne se mélange pas avec autres parfin

## Cumin
(bg) Кимион
(de) Krenzkümmel
(en) Cumin

- plus claire
- avec de tiges au bout de certaines graines
- utilisation dans la méditerrané, Inde et le proche orient
- en Bulgarie : pour la viande
- se mélange bien avec джоджен, чубрица, ...
