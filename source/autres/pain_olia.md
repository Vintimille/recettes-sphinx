# Pain «Olia»

## Ingrédients
- farine : 5 tasses
- eau tiède : 2 tasses
- levure sèche boulangère : 1 sachet
- sel : 5 pincées

## Temps
- préparation : 15 min + 15 min
- cuisson : 15 min

## Préparation

On fait un puis dans la farine, on met le sel au bord de la jatte, la levure dans le puis, avec un peu d'eau. On commence à malaxer le milieu et plus la boule se forme, plus on rajoute (un peu) d'eau. Une fois la pâte prête on la laisse reposer quelques heures à température ambiante.

On prépare les pains en essayant de mélanger le moins possible la pâte (en préservant autant que possible la fine croûte). On laisse reposer le temps de préchauffé le four à 250°. On fait cuire avec un récipient d'eau dans le four 15 minutes (à max).
