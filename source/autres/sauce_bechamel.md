# Sauce béchamel (au micro onde)

## Ingrédients
- lait : 25 cl
- beurre : 20 g
- farine : 2 cuillères à soupe

## Temps
- préparation : 10 min
- cuisson : 1 min + 3 min + 1 min

## Préparation

Faire fondre le beurre 1 minute au micro-ondes. Rajouter la farine en remuant bien. Porter le lait à ébullition, le laisser bouillir quelques instants. Verser le lait sur le mélange farine-beurre en fouettant. Assaisonner. Remettre le tout au micro-ondes pendant 1 minute. Bien fouetter, c'est prêt!
