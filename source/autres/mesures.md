# Les mesures à la cuillère

## 1 cuillère à café

- 5ml de liquide
- 6g de sucre en poudre
- 5g de sel
- 5g d'huile
- 5g de farine
- 4g de cacao en poudre

## 1 cuillère à soupe

- 15ml de liquide
- 15g de sucre en poudre
- 15g de sel
- 12g d'huile
- 12g de farine
- 10g de cacao en poudre
