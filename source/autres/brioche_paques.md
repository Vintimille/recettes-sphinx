# Brioche de Pâques bulgare (Kozounak)

## Ingrédients (pour 1 grand et 2 petits)

- farine : 1 kg
- levure boulangère fraîche : 50 g (2 x 25g)
- œufs : 6 + 1 jaune
- lait : 250 ml
- sucre : 300 (270+30) g
- écorce d'un citron
- vanille
- sel : 4g (= 4 pincé = une petite c.c.)
- gras : (225 g)
    - beurre : 150 g
    - saindoux : 50 g
    - huile : 25 g

## Pour la farce

- raisins secs, abricots secs, noix, amandes, confitures, ...

## Temps

- préparation : 3h30 (30 min + 1h30 + 30 min +1h30 + 40 min)
- cuisson : 40 min

## Préparation

Il faut mettre les ingrédients (farine, œufs) en température ambiante (40° programme «levage» du four, par exemple).

On réactive la levure (15 min): émietté avec 30 g de sucre et un peut d'eau (~150 ml) à 30°C-40°C. _(Il ne faut pas mettre à plus de 50°C et pas de sel.)_. Puis on mélange de la farine pour faire une consistance «mélangeable» avec le mixeur (pas trop dure).

On fait fondre au micro onde (1 1/2 min) le gras et on mélange avec le sel.

On bat avec le mixeur (à mettre dans l'ordre) : l'écorce de citron, les œufs, le sucre, le lait, la vanille. On commence à rajouter (tout en bâtant avec le mixeur) la farine, cuillère après cuillère. Quand la consistance devient un peu limite pour le mixeur (avant qu'il casse!) on rajoute la levure. On continue d'intégrer le reste de la farine en mélangeant à la cuillère à bois. À la fin on intègre 90% du gras en mélangeant à la main. Le reste du gras est pour quand on va mettre en formes.

On laisse monter (au four à 40°C programme «levage» pendant ~1h30). Pendant ce temps, on prépare les différentes faces.
On met dans des formes (la couronne + 1 moyen + 2 petits) avec de la farce, on décore avec et des noix/amandes. On laisse monter encore une fois au four à 40°C programme «levage» pendant ~1h30. On fait cuire au four à 180°C environ 40 min (20 min en couvrant au début, 10 min en découvert pour former la croûte, 10 min avant la fin on met le jaune d'œuf avec du sucre).

## Cuisson

À 180°C : 40 min (10 min avant la fin on badigeonne avec jaune d'œuf et sucre)

## Notes

1. En général on fait le double (2 kg de farine = 2 grands, 2 moyens et 2-4 petits).
2. Il faut laisser un peu de gras pour quand on met dans les formes.
3. Il ne faut pas mettre des farces trop liquides (pas de fruits frais ou surgelés).
4. On peut le faire aussi bien avec de la levure fraîche, qu'avec de la levure déshydraté.
